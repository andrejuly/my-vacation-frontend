import React, { useEffect, useState } from "react";
import { Button, Dropdown, Menu, Result, Timeline } from "antd";
import { DownOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { Link, useHistory, useLocation } from "react-router-dom";
import Cookies from "universal-cookie";

import Search from "antd/es/input/Search";
import { useTranslation } from "react-i18next";
import Timer from "../../../components/timer/timer";
import WeatherContainer from "../../weather/containers/weather";
import NewsContainer from "../../news/containers/news";
import NotesTableContainer from "../../note/containers/notes-contatiner";
import Spinner from "../../../components/spinner";

const SelectTrip = ({
  fetchUserTrips,
  fetchTrip,
  userTrips,
  selectTrip,
  trip,
  fetchTripById,
  loading,
  error,
}) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const history = useHistory();
  const [city, setCity] = useState("");
  const location = useLocation();
  const { t } = useTranslation();
  const cookies = new Cookies();

  useEffect(() => {
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    } else {
      history.push("/login");
    }
  }, [authorized, history]);

  useEffect(() => {
    if (userId) {
      fetchUserTrips(userId, searchValue);
    }
    const tripId = location.pathname.replace(/^.*?\/.*?\//, "");
    if (parseInt(tripId, 10) > 0 && userId) {
      setCity("");
      if (userId) {
        fetchTrip(tripId);
      }
    } else if (userId) {
      fetchTripById(userId);
    }
  }, [userId, location]); // eslint-disable-line react-hooks/exhaustive-deps

  const onClick = ({ key }) => {
    const result = {};
    result.tripname = key;
    result.userid = userId;
    setCity("");
    selectTrip(result, () => {
      setSearchValue("");
      fetchUserTrips(userId, "");
      fetchTripById(userId);
    });
  };
  if (loading) {
    return <Spinner />;
  }
  if (error)
    return (
      <Result
        status="500"
        title="500"
        subTitle="Sorry, something went wrong."
        extra={
          <Link to="/">
            <Button type="primary">Back Home</Button>
          </Link>
        }
      />
    );
  return trip && trip.name && userTrips ? (
    <div key="select-trip">
      <StyledRow key="row-select">
        <StyledText>{trip.name}</StyledText>
        <StyledSelect>
          <StyledTextSelect>{t("trip.myTrips")} &nbsp;</StyledTextSelect>
          <Dropdown
            placement="bottomRight"
            overlay={
              <Menu onClick={onClick}>
                <StyledSearch
                  id="search-all-trips-by-user"
                  placeholder={t("trip.find")}
                  allowClear
                  onSearch={(value) => {
                    fetchUserTrips(userId, value);
                    setSearchValue(value);
                  }}
                />
                {userTrips.map((el) => (
                  <Menu.Item key={el.name}>{el.name}</Menu.Item>
                ))}
              </Menu>
            }
          >
            <StyledTextSelect>
              {userTrips.map((el) => (el.activeTrip ? el.name : <div />))}
              <DownOutlined style={{ width: "30px" }} />
            </StyledTextSelect>
          </Dropdown>
        </StyledSelect>
      </StyledRow>
      <StyledContainer>
        <StyledColumnFirst>
          <StyledTimer>
            <StyledTextVacation>{t("timer.vacation")}</StyledTextVacation>
            <Timer startDate={trip.startDate} />
          </StyledTimer>
          <StyledWeather>
            <WeatherContainer city={city} />
          </StyledWeather>
        </StyledColumnFirst>

        <StyledColumnSecond>
          <StyledNews>
            <NewsContainer city={city} />
          </StyledNews>
          <StyledNotes>
            <NotesTableContainer />
          </StyledNotes>
        </StyledColumnSecond>

        <StyledColumnThird>
          <StyledTimeline>
            <Timeline>
              {trip.destinations.map((item) => (
                <Timeline.Item key={item.id}>
                  <StyledA
                    onClick={() => {
                      setCity(item.city);
                    }}
                  >
                    {item.city}
                  </StyledA>
                </Timeline.Item>
              ))}
            </Timeline>
          </StyledTimeline>
        </StyledColumnThird>
      </StyledContainer>

      <StyledRow />
    </div>
  ) : (
    <div style={{ height: "635px" }}>
      <StyledP>{t("trip.thereIsNoTrip")}</StyledP>
    </div>
  );
};

const StyledA = styled.a`
  color: black;
  background-color: transparent;
  outline: none;
  cursor: pointer;
  transition: color 0.3s;
  :hover {
    color: #1890ff;
  }
`;

const StyledSearch = styled(Search)`
  font-size: 18px;
  width: 90%;
  margin: 10px;
`;

const StyledText = styled.div`
  font-weight: 200;
  color: #1890ff;
  font-size: 26px;
`;

const StyledTextVacation = styled.div`
  font-size: 24px;
`;

const StyledTextSelect = styled.div`
  display: flex;
  margin-left: auto;
  margin-right: 0;
  align-content: baseline;
  align-items: baseline;
  font-weight: 200;
  font-size: 22px;
`;

const StyledP = styled.p`
  text-align: center;
  font-weight: 200;
  font-size: 26px;
`;

const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  padding-left: 5%;
  padding-right: 5%;
  padding-bottom: 5%;
`;

const StyledColumnFirst = styled.div`
  display: flex;
  flex-direction: column;
  border: #f0f0f0 1px solid;
  width: 35%;
`;

const StyledSelect = styled.div`
  display: flex;
  margin-left: auto;
  align-items: baseline;
`;

const StyledColumnSecond = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border: #f0f0f0 1px solid;
  width: 50%;
`;

const StyledColumnThird = styled.div`
  display: flex;
  flex-direction: column;
  border: #f0f0f0 1px solid;
  width: 20%;
`;

const StyledRow = styled.div`
  display: flex;
  flex-direction: row;
  padding-left: 5%;
  padding-right: 5%;
  margin-bottom: 20px;
`;

const StyledTimer = styled.div`
  width: 100%;
  height: 250px;
  border-bottom: #f0f0f0 1px solid;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const StyledNews = styled.div`
  width: 100%;
  height: 250px;
  border-bottom: #f0f0f0 1px solid;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledWeather = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledNotes = styled.div`
  width: 100%;
  padding: 3%;
`;

const StyledTimeline = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export default SelectTrip;
