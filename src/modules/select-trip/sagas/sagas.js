import { call, put, takeLatest, takeEvery } from "redux-saga/effects";
import {
  loadTripId,
  tripSelected,
  tripsLoaded,
  loadTrip,
  tripError,
  tripIdError,
} from "../actions";
import VacationService from "../../../services/vacation-service";

function* sagaWorker({ values, successAction }) {
  const service = new VacationService();
  try {
    const result = {};
    result.tripname = values.tripname;
    result.userid = values.userid;
    const trip = yield call(service.patchTrip, result);
    yield put(tripSelected(trip));
    successAction();
  } catch (error) {
    console.log(error);
  }
}

export function* tripSelectWatcher() {
  yield takeLatest("SELECT_TRIP", sagaWorker);
}

function* sagaTripIdWorker({ userId }) {
  const service = new VacationService();
  try {
    const result = {};
    result.userid = userId;
    const payload = yield call(service.getTripFromUserId, result);
    yield put(loadTrip(payload.data));
  } catch (error) {
    yield put(tripIdError(error));
  }
}

export function* tripIdSelectWatcher() {
  yield takeLatest("FETCH_TRIP_ID_REQUEST", sagaTripIdWorker);
}

function* sagaTripWorker({ tripId }) {
  const service = new VacationService();
  try {
    const trip = yield call(service.getTripById, tripId);
    yield put(loadTripId(trip.data));
  } catch (error) {
    yield put(tripError(error));
  }
}

export function* mainPageWatcher() {
  yield takeEvery("FETCH_TRIP_REQUEST", sagaTripWorker);
}

function* sagaUserTripsWorker({ userId, keyword }) {
  const vacationService = new VacationService();
  try {
    const result = {};
    result.userid = userId;
    result.keyword = keyword;
    const payload = yield call(vacationService.getTripsFromUserId, result);
    yield put(tripsLoaded(payload.data, payload.data.keyword));
  } catch (error) {
    yield put(tripError(error));
  }
}

export function* userTripsWatcher() {
  yield takeLatest("FETCH_USERTRIPS_REQUEST", sagaUserTripsWorker);
}
