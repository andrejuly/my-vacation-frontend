import { connect } from "react-redux";
import {
  requestTripId,
  selectTrip,
  tripsRequested,
  requestTrip,
} from "../actions";
import SelectTrip from "../components/selectTrip";

const mapStateToProps = ({
  tripSelect: { trip, userTrips, loading, error },
}) => ({
  trip,
  userTrips,
  loading,
  error,
});

const mapDispatchToProps = (dispatch) => ({
  fetchTrip: (tripId) => {
    dispatch(requestTrip(tripId));
  },
  selectTrip: (values, successAction) => {
    dispatch(selectTrip(values, successAction));
  },
  fetchUserTrips: (userId, keyword) =>
    dispatch(tripsRequested(userId, keyword)),
  fetchTripById: (tripId) => {
    dispatch(requestTripId(tripId));
  },
});

const SelectTripContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectTrip);

export default SelectTripContainer;
