const initialState = {
  userTrips: [],
  trip: {},
  loading: true,
  error: null,
};

const selectTripReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_TRIP_REQUEST":
      return {
        ...state,
        trip: {},
        loading: true,
        error: null,
      };
    case "FETCH_TRIP_ID_REQUEST":
      return {
        ...state,
        trip: {},
        loading: true,
        error: null,
      };
    case "FETCH_TRIP_FAILURE":
      return {
        ...state,
        trip: {},
        loading: false,
        error: action.error,
      };
    case "FETCH_TRIP_ID_FAILURE":
      return {
        ...state,
        trip: {},
        loading: false,
        error: action.error,
      };
    case "FETCH_TRIP_SUCCESS":
      return {
        ...state,
        trip: action.trip,
        loading: false,
        error: null,
      };
    case "FETCH_TRIP_ID_SUCCESS":
      return {
        ...state,
        trip: action.trip,
        loading: false,
        error: null,
      };

    case "TRIP_SELECTED":
      return {
        ...state,
        userTrips: action.userTrips,
      };
    case "FETCH_USERTRIPS_REQUEST":
      return {
        ...state,
        userTrips: [],
      };
    case "FETCH_USERTRIPS_SUCCESS":
      return {
        ...state,
        userTrips: action.userTrips,
      };
    default:
      return state;
  }
};

export default selectTripReducer;
