export const requestTrip = (tripId) => ({
  type: "FETCH_TRIP_REQUEST",
  tripId,
});

export const loadTrip = (trip) => ({
  type: "FETCH_TRIP_SUCCESS",
  trip,
});

export const tripError = (error) => ({
  type: "FETCH_TRIP_FAILURE",
  error,
});

export const tripIdError = (error) => ({
  type: "FETCH_TRIP_ID_FAILURE",
  error,
});

export const requestTripId = (userId) => ({
  type: "FETCH_TRIP_ID_REQUEST",
  userId,
});

export const loadTripId = (trip) => ({
  type: "FETCH_TRIP_ID_SUCCESS",
  trip,
});

export const selectTrip = (values, successAction) => ({
  type: "SELECT_TRIP",
  values,
  successAction,
});

export const tripSelected = (trip) => ({
  type: "TRIP_SELECTED",
  trip,
});

export const tripsLoaded = (userTrips, keyword) => ({
  type: "FETCH_USERTRIPS_SUCCESS",
  userTrips,
  keyword,
});

export const tripsRequested = (userId, keyword) => ({
  type: "FETCH_USERTRIPS_REQUEST",
  userId,
  keyword,
});
