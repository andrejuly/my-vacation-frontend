const newsLoaded = (news) => ({
  type: "FETCH_NEWS_SUCCESS",
  news,
});

const newsRequested = (tripId, city) => ({
  type: "FETCH_NEWS_REQUEST",
  tripId,
  city,
});

const newsError = (error) => ({
  type: "FETCH_NEWS_FAILURE",
  error,
});

export { newsRequested, newsError, newsLoaded };
