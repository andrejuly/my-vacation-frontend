import React, { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import { useHistory } from "react-router-dom";
import { Empty } from "antd";
import News from "../../../components/news/news";

import Spinner from "../../../components/spinner";

const NewsComponent = (props) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const history = useHistory();

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    } else {
      history.push("/login");
    }
  }, [history, authorized]);

  const { news, loading, error, city, tripId } = props;

  useEffect(() => {
    if (userId) {
      props.fetchNews(tripId, city);
    }
  }, [userId, city, tripId]); // eslint-disable-line react-hooks/exhaustive-deps

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
  }
  return <News data={news} />;
};

export default NewsComponent;
