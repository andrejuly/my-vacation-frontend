import { call, put, takeLatest } from "redux-saga/effects";
import { newsLoaded, newsError } from "../actions";
import VacationService from "../../../services/vacation-service";

function* sagaWorker({ tripId, city }) {
  const vacationService = new VacationService();
  try {
    const result = {};
    result.tripid = tripId;
    result.city = city;
    const payload = yield call(vacationService.getNewsFromUserId, result);
    yield put(newsLoaded(payload.data));
  } catch (error) {
    yield put(newsError(error));
  }
}

export function* newsWatcher() {
  yield takeLatest("FETCH_NEWS_REQUEST", sagaWorker);
}
