const initialState = {
  data: {},
  loading: true,
  error: null,
};

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_NEWS_REQUEST":
      return {
        ...state,
        data: {},
        loading: true,
        error: null,
      };
    case "FETCH_NEWS_SUCCESS":
      return {
        ...state,
        data: action.news,
        loading: false,
        error: null,
      };
    case "FETCH_NEWS_FAILURE":
      return {
        ...state,
        data: {},
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default newsReducer;
