import { connect } from "react-redux";
import { newsRequested } from "../actions";
import NewsComponent from "../components/news-component";

const mapStateToProps = (state, props) => ({
  news: state.news.data.articles,
  tripId: state.tripSelect.trip.id,
  loading: state.news.loading,
  error: state.news.error,
  city: props.city,
});

const mapDispatchToProps = (dispatch) => ({
  fetchNews: (tripId, city) => dispatch(newsRequested(tripId, city)),
});

const NewsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsComponent);
export default NewsContainer;
