import { combineReducers } from "redux";
import weatherReducer from "./weather/reducers";
import tripsReducer from "./trips-table/reducers";
import loginPageReducer from "./login-page/reducers/login-page-reducer";
import newsReducer from "./news/reducers";
import mainPageReducer from "./main-page/reducers/main-page-reducer";
import createTripReducer from "./create-trip/reducers/create-trip-reducer";
import signupPageReducer from "./register-page/reducers";
import mapReducer from "./map/reducers/main-page-reducer";
import selectTripReducer from "./select-trip/reducers";
import notesReducer from "./note/reducers";
import subscriptionsReducer from "./subscription-list/reducers";

const rootReducer = combineReducers({
  weather: weatherReducer,
  trips: tripsReducer,
  loginPage: loginPageReducer,
  news: newsReducer,
  mainPage: mainPageReducer,
  createTrip: createTripReducer,
  registerPage: signupPageReducer,
  destinations: mapReducer,
  tripSelect: selectTripReducer,
  notes: notesReducer,
  subscriptions: subscriptionsReducer,
});

export default rootReducer;
