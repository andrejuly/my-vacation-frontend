import React, { useEffect, useMemo, useState } from "react";
import Cookies from "universal-cookie";
import {
  Alert,
  AutoComplete,
  Button,
  DatePicker,
  Form,
  Input,
  Modal,
  Space,
  Switch,
} from "antd";

import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

const CreateTrip = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const history = useHistory();
  const { t } = useTranslation();

  useEffect(() => {
    props.fetchCities();
  }, [authorized]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    } else {
      history.push("/login");
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const { cities, errors } = props;

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const options = useMemo(() => {
    const city = cities || [];
    return city.map((item) => ({
      value: `${item.name} | ${item.country} (${item.coord.lat},${item.coord.lon})`,
    }));
  }, [cities]);

  const onFinish = (val) => {
    const values = { ...val };
    values.user = { id: userId };
    values.destinations = values.destinations.map((el) => ({
      city: el.city.slice(0, el.city.indexOf(" |")),
      lat: parseFloat(
        el.city.slice(el.city.indexOf("(") + 1, el.city.indexOf(","))
      ),
      lon: parseFloat(
        el.city.slice(el.city.indexOf(",") + 1, el.city.indexOf(")"))
      ),
    }));
    props.createTrip(values);
    if (errors) {
      setIsModalVisible(false);
    } else {
      history.push("/");
    }
  };

  return (
    <>
      <StyledButton type="primary" onClick={showModal}>
        {t("createTrip.title")}
      </StyledButton>
      <Modal
        title={t("createTrip.title")}
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          name="dynamic_form_nest_item"
          onFinish={onFinish}
          layout="vertical"
          autoComplete="on"
        >
          <Form.Item name="name" label={t("createTrip.name")}>
            <Input style={{ width: "70%" }} />
          </Form.Item>

          <Form.Item name="startDate" label={t("createTrip.startDate")}>
            <DatePicker placeholder="" />
          </Form.Item>

          <Form.Item name="endDate" label={t("createTrip.endDate")}>
            <DatePicker placeholder="" />
          </Form.Item>
          <Form.List name="destinations">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, fieldKey, ...restField }) => (
                  <StyledSpace key={key} align="baseline">
                    <Form.Item
                      {...restField}
                      name={[name, "city"]}
                      fieldKey={[fieldKey, "first"]}
                      rules={[
                        {
                          required: true,
                          message: t("createTrip.alertCity"),
                        },
                      ]}
                    >
                      <AutoComplete
                        style={{
                          width: 400,
                        }}
                        options={options}
                        placeholder={t("createTrip.placeholderCity")}
                        filterOption={(inputValue, option) =>
                          option.value
                            .toUpperCase()
                            .indexOf(inputValue.toUpperCase()) !== -1
                        }
                      />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </StyledSpace>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    {t("createTrip.points")}
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          <Form.Item name="activeTrip" label={t("createTrip.activeTrip")}>
            <Switch />
          </Form.Item>
          <Form.Item>
            <StyledButtonSave type="primary" htmlType="submit">
              {t("createTrip.button")}
            </StyledButtonSave>
          </Form.Item>
        </Form>
        {errors && (
          <StyledAlert
            message={t("createTrip.sameTrip")}
            type="error"
            showIcon
          />
        )}
      </Modal>
    </>
  );
};

const StyledButton = styled(Button)`
  margin: 0 30px;
`;

const StyledButtonSave = styled(Button)`
  margin-left: 183px;
  width: 100px;
`;

const StyledAlert = styled(Alert)`
  height: 50px;
  border: 1px solid #c4c4c4;
  font-size: 14px;
  font-style: normal;
`;

const StyledSpace = styled(Space)`
  display: flex;
  margin-bottom: 8px;
`;

export default CreateTrip;
