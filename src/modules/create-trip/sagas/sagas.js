import { call, put, takeLatest } from "redux-saga/effects";
import { createTripError, loadCities, tripCreates } from "../actions";
import VacationService from "../../../services/vacation-service";

const service = new VacationService();

function* sagaCitiesWorker() {
  try {
    const payload = yield call(service.getCitiesForTrip, {});
    yield put(loadCities(payload.data));
  } catch (error) {
    console.log(error);
  }
}

function* sagaCreateTripWorker({ trip }) {
  try {
    const payload = yield call(service.addTrip, trip);
    yield put(tripCreates(payload));
  } catch (error) {
    yield put(createTripError(error));
  }
}

export function* createTripWatcher() {
  yield takeLatest("FETCH_CITIES_REQUEST", sagaCitiesWorker);
  yield takeLatest("CREATE_TRIP", sagaCreateTripWorker);
}
