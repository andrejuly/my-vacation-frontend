import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { createTrip, requestCities } from "../actions";
import CreateTrip from "../components/create-trip";

const mapStateToProps = ({ createTrip: { cities, errors } }) => ({
  cities,
  errors,
});

const mapDispatchToProps = (dispatch) => ({
  fetchCities: () => {
    dispatch(requestCities());
  },
  createTrip: (trip) => {
    dispatch(createTrip(trip));
  },
});

const CreateTripContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTrip);

export default withRouter(CreateTripContainer);
