export const requestCities = () => ({
  type: "FETCH_CITIES_REQUEST",
});

export const loadCities = (cities) => ({
  type: "FETCH_CITIES_SUCCESS",
  cities,
});

export const createTrip = (trip) => ({
  type: "CREATE_TRIP",
  trip,
});

export const tripCreates = (trip) => ({
  type: "TRIP_CREATED",
  trip,
});

export const createTripError = () => ({
  type: "CREATE_TRIP_ERROR",
});
