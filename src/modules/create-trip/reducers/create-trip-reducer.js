const initialState = {
  cities: null,
  trip: null,
  errors: false,
};

const createTripReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_CITIES_SUCCESS":
      return {
        ...state,
        cities: action.cities,
      };
    case "TRIP_CREATED":
      return {
        ...state,
        trip: action.trip,
      };
    case "CREATE_TRIP_ERROR":
      return {
        ...state,
        errors: true,
      };
    default:
      return state;
  }
};

export default createTripReducer;
