const loadTrips = (trips, pageNumber, total, filter, sortDir, selectValue) => ({
  type: "FETCH_TRIPS_SUCCESS",
  trips,
  pageNumber,
  total,
  filter,
  sortDir,
  selectValue,
});

const requestTrips = (userId, pageNumber, filter, sortDir, selectValue) => ({
  type: "FETCH_TRIPS_REQUEST",
  userId,
  pageNumber,
  filter,
  sortDir,
  selectValue,
});

const errorTrips = (error) => ({
  type: "FETCH_TRIPS_FAILURE",
  error,
});

export const followUserRequest = (
  userId,
  subscribedTo,
  pageNumber,
  filter,
  sortDir,
  selectValue
) => ({
  type: "FOLLOW_REQUEST",
  userId,
  subscribedTo,
  pageNumber,
  filter,
  sortDir,
  selectValue,
});

export const unfollowUserSuccess = () => ({
  type: "UNFOLLOW_SUCCESS",
});

export const unfollowUserRequest = (
  userId,
  subscribedTo,
  pageNumber,
  filter,
  sortDir,
  selectValue
) => ({
  type: "UNFOLLOW_REQUEST",
  userId,
  subscribedTo,
  pageNumber,
  filter,
  sortDir,
  selectValue,
});

export const followUserSuccess = () => ({
  type: "FOLLOW_SUCCESS",
});

export const requestUserinfo = (username) => ({
  type: "FETCH_USERINFO_REQUEST",
  username,
});

export const loadUserinfo = (userinfo) => ({
  type: "FETCH_USERINFO_SUCCESS",
  userinfo,
});

export { loadTrips, errorTrips, requestTrips };
