import { connect } from "react-redux";
import {
  requestTrips,
  requestUserinfo,
  followUserRequest,
  unfollowUserRequest,
} from "../actions";
import TripsTable from "../components/trips-table";

const mapStateToProps = ({
  trips: { trips, total, userinfo, pageNumber },
}) => ({
  trips,
  total,
  userinfo,
  pageNumber,
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: (userId, pageNumber, filter, sortDir, selectValue) => {
    dispatch(requestTrips(userId, pageNumber, filter, sortDir, selectValue));
  },
  followUser: (
    userId,
    subscribedTo,
    pageNumber,
    filter,
    sortDir,
    selectValue
  ) => {
    dispatch(
      followUserRequest(
        userId,
        subscribedTo,
        pageNumber,
        filter,
        sortDir,
        selectValue
      )
    );
  },
  unfollowUser: (
    userId,
    subscribedTo,
    pageNumber,
    filter,
    sortDir,
    selectValue
  ) => {
    dispatch(
      unfollowUserRequest(
        userId,
        subscribedTo,
        pageNumber,
        filter,
        sortDir,
        selectValue
      )
    );
  },
  fetchUserInfo: (username) => {
    dispatch(requestUserinfo(username));
  },
});

const TripsTableContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TripsTable);

export default TripsTableContainer;
