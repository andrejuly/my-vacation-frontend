import { call, put, takeLatest } from "redux-saga/effects";
import {
  errorTrips,
  loadTrips,
  followUserSuccess,
  unfollowUserSuccess,
  loadUserinfo,
} from "../actions";
import VacationService from "../../../services/vacation-service";

const vacationService = new VacationService();

function* sagaWorker({ userId, pageNumber, filter, sortDir, selectValue }) {
  try {
    const result = {};
    result.userid = userId;
    result.page = pageNumber - 1;
    result.filter = filter;
    result.sort = sortDir;
    result.type = selectValue;
    const payload = yield call(vacationService.getTrips, result);
    yield put(
      loadTrips(
        payload.data.content,
        payload.data.pageable.pageNumber,
        payload.data.totalElements,
        filter,
        sortDir,
        selectValue
      )
    );
  } catch (error) {
    yield put(errorTrips(error));
  }
}

export function* tripsWatcher() {
  yield takeLatest("FETCH_TRIPS_REQUEST", sagaWorker);
}

function* sagaUserInfoWorker({ username }) {
  try {
    const result = {};
    result.username = username;
    const payload = yield call(vacationService.getUserInfo, result);
    yield put(loadUserinfo(payload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* userInfoWatcher() {
  yield takeLatest("FETCH_USERINFO_REQUEST", sagaUserInfoWorker);
}

function* followUserWorker({
  userId,
  subscribedTo,
  pageNumber,
  filter,
  sortDir,
  selectValue,
}) {
  try {
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(vacationService.follow, result);
    yield put(followUserSuccess());
    const updateResult = {};
    updateResult.userid = userId;
    updateResult.page = pageNumber - 1;
    updateResult.filter = filter;
    updateResult.sort = sortDir;
    updateResult.type = selectValue;
    const payload = yield call(vacationService.getTrips, updateResult);
    yield put(
      loadTrips(
        payload.data.content,
        payload.data.pageable.pageNumber,
        payload.data.totalElements,
        filter,
        sortDir,
        selectValue
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* unfollowUserWorker({
  userId,
  subscribedTo,
  pageNumber,
  filter,
  sortDir,
  selectValue,
}) {
  try {
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(vacationService.unfollow, result);
    yield put(unfollowUserSuccess());
    const updateResult = {};
    updateResult.userid = userId;
    updateResult.page = pageNumber - 1;
    updateResult.filter = filter;
    updateResult.sort = sortDir;
    updateResult.type = selectValue;
    const payload = yield call(vacationService.getTrips, updateResult);
    yield put(
      loadTrips(
        payload.data.content,
        payload.data.pageable.pageNumber,
        payload.data.totalElements,
        filter,
        sortDir,
        selectValue
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* followUserWatcher() {
  yield takeLatest("FOLLOW_REQUEST", followUserWorker);
}

export function* unfollowUserWatcher() {
  yield takeLatest("UNFOLLOW_REQUEST", unfollowUserWorker);
}
