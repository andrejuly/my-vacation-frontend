const initialState = {
  trips: [],
  userinfo: {},
  pageNumber: 1,
  total: 0,
  loading: false,
  error: null,
};

const tripsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_TRIPS_SUCCESS":
      return {
        ...state,
        trips: action.trips,
        pageNumber: action.pageNumber,
        total: action.total,
        loading: false,
        error: null,
      };
    case "FETCH_TRIPS_FAILURE":
      return {
        ...state,
        trips: [],
        pageNumber: 1,
        total: 0,
        loading: false,
        error: action.error,
      };
    case "FETCH_USERINFO_SUCCESS":
      return {
        ...state,
        userinfo: action.userinfo,
      };
    case "FOLLOW_SUCCESS":
      return {
        ...state,
      };
    case "UNFOLLOW_SUCCESS":
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default tripsReducer;
