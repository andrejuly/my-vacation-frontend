import React, { useEffect, useState } from "react";
import Search from "antd/es/input/Search";
import styled from "styled-components";
import { Button, Modal, Table, Radio } from "antd";
import Cookies from "universal-cookie/";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const TripsTable = ({
  fetchData,
  trips,
  total,
  followUser,
  unfollowUser,
  fetchUserInfo,
  userinfo,
}) => {
  const [pageNumber, setPageNumber] = useState(1);
  const [sort, setSort] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [selectValue, setSelectValue] = useState("all");
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const [modalTitle, setModalTitle] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const history = useHistory();
  const { t } = useTranslation();
  const cookies = new Cookies();
  const showModal = (userName) => {
    setModalTitle(userName);
    fetchUserInfo(modalTitle);
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const columns = [
    {
      title: t("trips.name"),
      dataIndex: "name",
      align: "center",
      key: "name",
      render: (name) => <StyledA>{name}</StyledA>,
      onCell: (record) => ({
        onClick: () => {
          history.push(`/trip/${record.id}`);
        },
      }),
    },
    {
      title: t("trips.startDate"),
      dataIndex: "startDate",
      key: "startDate",
      align: "center",
      render: (startDate) => (
        <>{new Date(startDate).toLocaleDateString("en-GB")}</>
      ),
      sorter: true,
    },
    {
      title: t("trips.endDate"),
      dataIndex: "endDate",
      key: "endDate",
      align: "center",
      render: (endDate) => <>{new Date(endDate).toLocaleDateString("en-GB")}</>,
      sorter: true,
    },
    {
      title: t("trips.username"),
      dataIndex: "user",
      key: "user",
      align: "center",
      render: (key) => (
        <StyledA
          onClick={() => {
            showModal(key.userName);
          }}
        >
          {/* eslint-disable-next-line react/destructuring-assignment */}
          {key.userName}
        </StyledA>
      ),
    },
    {
      title: t("trips.join"),
      key: "action",
      align: "center",
      render: (item) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          {/* eslint-disable-next-line no-nested-ternary,react/destructuring-assignment */}
          {authorized && !(item.user.id === userId) ? (
            // eslint-disable-next-line react/destructuring-assignment
            !item.subscribe ? (
              <StyledButton
                onClick={() => {
                  followUser(
                    userId,
                    item.id,
                    pageNumber,
                    searchValue,
                    sort,
                    selectValue
                  );
                }}
              >
                {t("trips.subscribe")}
              </StyledButton>
            ) : (
              <StyledRemoveButton
                onClick={() => {
                  unfollowUser(
                    userId,
                    item.id,
                    pageNumber,
                    searchValue,
                    sort,
                    selectValue
                  );
                }}
              >
                {t("trips.unsubscribe")}
              </StyledRemoveButton>
            )
          ) : null}
        </div>
      ),
    },
  ];

  useEffect(() => {
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    }
    if (userId) {
      fetchData(userId, pageNumber, "", "", selectValue);
    }
    if (userId) {
      fetchUserInfo(userId, pageNumber, "", "");
    }
  }, [authorized, userId]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (userId) {
      fetchUserInfo(modalTitle);
    }
  }, [modalTitle]); // eslint-disable-line react-hooks/exhaustive-deps

  const onChange = (pagination, filter, sorter) => {
    if (sorter.field !== undefined && sorter.order !== undefined) {
      setSort(`${sorter.field},${sorter.order.toString().slice(0, -3)}`);
    }
    fetchData(userId, pagination.current, searchValue, sort, selectValue);
    setPageNumber(pagination.current);
  };

  return (
    <div style={{ marginLeft: "16px", height: "635px" }}>
      <StyledHeaderTab>
        <StyledSearch
          id="search-trips"
          placeholder={t("trip.find")}
          allowClear
          onSearch={(value) => {
            fetchData(userId, 1, value, "", selectValue);
            setPageNumber(1);
            setSearchValue(value);
          }}
        />
        <Radio.Group value={selectValue} style={{ marginLeft: "20px" }}>
          <Radio
            value="all"
            onClick={() => {
              setSelectValue("all");
              fetchData(userId, 1, searchValue, "", "all");
              setPageNumber(1);
            }}
          >
            {t("trips.all")}
          </Radio>
          <Radio
            value="favorites"
            onClick={() => {
              setSelectValue("favorites");
              fetchData(userId, 1, searchValue, "", "favorites");
              setPageNumber(1);
            }}
          >
            {t("trips.favorites")}
          </Radio>
        </Radio.Group>
      </StyledHeaderTab>

      <StyledTable
        columns={columns}
        pagination={{ current: pageNumber, total, pageSize: 6 }}
        dataSource={trips}
        onChange={onChange}
        rowKey={trips.id}
      />
      <Modal
        title={modalTitle}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        {userinfo.fname || userinfo.sex ? (
          <div>
            <p>
              {t("trips.userfname")} {userinfo.fname}
            </p>
            <p>
              {t("trips.gender")} {userinfo.sex}
            </p>
          </div>
        ) : (
          <></>
        )}
        {userinfo.birthDate !== null ? (
          <p>
            {t("trips.birthDate")}
            {new Date(userinfo.birthDate).toLocaleDateString("en-GB")}
          </p>
        ) : (
          <></>
        )}
      </Modal>
    </div>
  );
};

const StyledA = styled.a`
  color: black;
  background-color: transparent;
  outline: none;
  cursor: pointer;
  transition: color 0.3s;
  :hover {
    color: #1890ff;
  }
`;

const StyledSearch = styled(Search)`
  font-size: 18px;
  width: 50%;
`;

const StyledHeaderTab = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 1%;
`;

const StyledTable = styled(Table)`
  padding-right: 16px;
  font-size: 16px;
`;

const StyledButton = styled(Button)`
  border: 1px solid;
  border-radius: 5px;
  color: #1890ff;
  width: 150px;
  :hover {
    border: 1px solid;
    color: white;
    background: #1890ff;
  }
`;

const StyledRemoveButton = styled(Button)`
  border: 1px solid;
  border-radius: 5px;
  color: red;
  width: 150px;
  :hover {
    border: 1px solid red;
    color: white;
    background: red;
  }
`;

export default TripsTable;
