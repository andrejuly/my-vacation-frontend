export const requestSubscription = (userId, pageNumber, keyword) => ({
  type: "FETCH_SUBSCRIPTIONS_REQUEST",
  userId,
  pageNumber,
  keyword,
});

export const loadSubscription = (
  subscriptions,
  pageNumber,
  total,
  keyword
) => ({
  type: "FETCH_SUBSCRIPTIONS_SUCCESS",
  subscriptions,
  pageNumber,
  total,
  keyword,
});

export const errorSubscription = (error) => ({
  type: "FETCH_SUBSCRIPTIONS_FAILURE",
  error,
});
