import { call, put, takeLatest } from "redux-saga/effects";
import VacationService from "../../../services/vacation-service";
import { errorSubscription, loadSubscription } from "../actions";

const vacationService = new VacationService();

function* sagaWorker({ userId, pageNumber, keyword }) {
  try {
    const result = {};
    result.userid = userId;
    result.page = pageNumber - 1;
    result.keyword = keyword;
    const payload = yield call(vacationService.getSubscriptions, result);
    yield put(
      loadSubscription(
        payload.data.content,
        payload.data.pageable.pageNumber,
        payload.data.totalElements,
        payload.data.keyword
      )
    );
  } catch (error) {
    yield put(errorSubscription(error));
  }
}

export function* subscriptionsWatcher() {
  yield takeLatest("FETCH_SUBSCRIPTIONS_REQUEST", sagaWorker);
}
