import React, { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import { List } from "antd";
import { Icon } from "@iconify/react";
import palmTree from "@iconify/icons-emojione-monotone/palm-tree";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Search from "antd/es/input/Search";
import { useTranslation } from "react-i18next";

const SubscriptionsList = ({ fetchData, subscriptions, total }) => {
  const [pageNumber, setPageNumber] = useState(1);
  const [searchValue, setSearchValue] = useState("");
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const { t } = useTranslation();

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    }
    if (userId) {
      fetchData(userId, pageNumber, searchValue);
    }
  }, [authorized, userId]); // eslint-disable-line react-hooks/exhaustive-deps
  const onChange = (page) => {
    fetchData(userId, page, searchValue);
    setPageNumber(page);
  };

  return (
    <List
      header={
        <>
          <StyledP>{t("sub.subscriptions")}</StyledP>
          <StyledSearch
            id="search-trips"
            placeholder={t("sub.find")}
            allowClear
            onSearch={(value) => {
              fetchData(userId, 1, value);
              setPageNumber(1);
              setSearchValue(value);
            }}
          />
        </>
      }
      itemLayout="horizontal"
      dataSource={subscriptions}
      pagination={{
        current: pageNumber,
        total,
        pageSize: 6,
        onChange,
      }}
      renderItem={(item) => (
        <List.Item style={{ alignItems: "center" }}>
          <List.Item.Meta
            avatar={
              <Icon
                icon={palmTree}
                style={{
                  fontSize: "26px",
                  color: "black",
                  background: "white",
                }}
              />
            }
            title={<Link to={`/trip/${item.id}`}>{item.name}</Link>}
          />
        </List.Item>
      )}
    />
  );
};

const StyledP = styled.p`
  text-align: center;
  font-weight: 200;
  font-size: 18px;
`;

const StyledSearch = styled(Search)`
  font-size: 18px;
  width: 100%;
  margin-bottom: 1%;
`;

export default SubscriptionsList;
