import { connect } from "react-redux";
import { requestSubscription } from "../actions";
import SubscriptionsList from "../components/subscriptions-list";

const mapStateToProps = ({
  subscriptions: { subscriptions, total, pageNumber },
}) => ({
  subscriptions,
  total,
  pageNumber,
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: (userId, pageNumber, keyword) => {
    dispatch(requestSubscription(userId, pageNumber, keyword));
  },
});

const SubscriptionsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SubscriptionsList);

export default SubscriptionsContainer;
