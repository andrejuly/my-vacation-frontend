const initialState = {
  subscriptions: [],
  pageNumber: 1,
  total: 0,
  loading: false,
  error: null,
};

const subscriptionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_SUBSCRIPTIONS_SUCCESS":
      return {
        ...state,
        subscriptions: action.subscriptions,
        pageNumber: action.pageNumber,
        total: action.total,
        loading: false,
        error: null,
      };
    case "FETCH_SUBSCRIPTIONS_FAILURE":
      return {
        ...state,
        subscriptions: [],
        pageNumber: 1,
        total: 0,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default subscriptionsReducer;
