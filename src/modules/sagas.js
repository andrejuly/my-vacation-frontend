import { all } from "redux-saga/effects";
import { weatherWatcher } from "./weather/sagas/sagas";
import { newsWatcher } from "./news/sagas/sagas";
import {
  followUserWatcher,
  tripsWatcher,
  unfollowUserWatcher,
  userInfoWatcher,
} from "./trips-table/sagas/sagas";
import { loginPageWatcher } from "./login-page/sagas/sagas";
import { userNameWatcher } from "./main-page/sagas/sagas";
import { createTripWatcher } from "./create-trip/sagas/sagas";
import { signupWatcher } from "./register-page/sagas/sagas";
import { mapWatcher } from "./map/sagas/sagas";
import {
  mainPageWatcher,
  tripSelectWatcher,
  userTripsWatcher,
  tripIdSelectWatcher,
} from "./select-trip/sagas/sagas";
import { notesWatcher, sendNotesWatcher } from "./note/sagas/sagas";
import { subscriptionsWatcher } from "./subscription-list/sagas/sagas";

export function* rootWatcher() {
  yield all([
    weatherWatcher(),
    tripsWatcher(),
    loginPageWatcher(),
    newsWatcher(),
    mainPageWatcher(),
    createTripWatcher(),
    signupWatcher(),
    userNameWatcher(),
    mapWatcher(),
    tripSelectWatcher(),
    userTripsWatcher(),
    followUserWatcher(),
    unfollowUserWatcher(),
    notesWatcher(),
    sendNotesWatcher(),
    subscriptionsWatcher(),
    userInfoWatcher(),
    tripIdSelectWatcher(),
  ]);
}
