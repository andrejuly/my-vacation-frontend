import React from "react";
import styled from "styled-components";

import { Form, Input, Button, Alert } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const LoginPage = ({ login, errors }) => {
  const history = useHistory();
  const { t } = useTranslation();

  const onFinish = (values) => {
    login(values, () => {
      history.push("/trip");
    });
  };

  return (
    <StyledLogin>
      <StyledFormLogin
        className="login-form"
        name="normal_login"
        onFinish={onFinish}
      >
        <Form.Item
          name="login"
          rules={[
            {
              required: true,
              message: t("login.alertLogin"),
            },
          ]}
        >
          <StyledInput
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder={t("login.login")}
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: t("login.alertPassword"),
            },
          ]}
        >
          <StyledInput
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder={t("login.password")}
          />
        </Form.Item>

        <Form.Item>
          <StyledButton
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            {t("login.button")}
          </StyledButton>
          <StyledText>
            <p>
              {t("login.or")}
              <Link to="/registration">{t("login.register")}</Link>
            </p>
          </StyledText>
        </Form.Item>
        {errors && (
          <StyledAlert message={t("login.error")} type="error" showIcon />
        )}
      </StyledFormLogin>
    </StyledLogin>
  );
};

const StyledLogin = styled.div`
  font-family: "Montserrat", sans-serif;
  font-weight: 100;
  height: 100vh;
  background: url("https://trip-dreams.ru/wp-content/uploads/Marshrut-puteshestviya-po-Evrope.jpg")
    30% fixed;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  min-height: 100%;
  padding: 20px;
`;

const StyledFormLogin = styled(Form)`
  border-radius: 2px 2px 5px 5px;
  padding: 20px 20px 10px 20px;
  width: 90%;
  max-width: 320px;
  background: #ffffff;
  position: relative;
  padding-bottom: 3%;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3);
`;

const StyledInput = styled(Input)`
  height: 50px;
  border: 1px solid #c4c4c4;
  font-size: 18px;
  font-style: normal;
`;

const StyledButton = styled(Button)`
  border-radius: 20px;
  font-size: 16px;
  width: 200px;
  height: 2rem;
  margin-left: 40px;
`;

const StyledAlert = styled(Alert)`
  height: 50px;
  border: 1px solid #c4c4c4;
  font-size: 14px;
  font-style: normal;
`;

const StyledText = styled.div`
  text-align: center;
  margin-top: 15px;
`;

export default LoginPage;
