import { takeLatest, call, put } from "redux-saga/effects";
import Cookies from "universal-cookie";
import { loginError, userLoggedIn } from "../actions";
import VacationService from "../../../services/vacation-service";

function* sagaWorker({ values, successAction }) {
  const service = new VacationService();
  const cookies = new Cookies();
  try {
    const user = yield call(service.login, values);
    const { id, login, token } = user.data;
    yield put(userLoggedIn(id, login, token));
    cookies.set("token", token, { maxAge: 3600 });
    cookies.set("user_id", id, { maxAge: 3600 });
    successAction();
  } catch (error) {
    yield put(loginError(error));
  }
}

export function* loginPageWatcher() {
  yield takeLatest("LOGIN_USER", sagaWorker);
}
