export const loginUser = (values, successAction) => ({
  type: "LOGIN_USER",
  values,
  successAction,
});

export const userLoggedIn = (userId, login, token) => ({
  type: "USER_LOGGED_IN",
  userId,
  login,
  token,
});

export const loginError = () => ({
  type: "LOGIN_ERROR",
});
