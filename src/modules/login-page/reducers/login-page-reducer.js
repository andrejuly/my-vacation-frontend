const initialState = {
  userId: null,
  login: null,
  token: null,
  authorized: false,
  errors: false,
};

const loginPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "USER_LOGGED_IN":
      return {
        ...state,
        userId: action.userId,
        login: action.login,
        token: action.token,
        authorized: true,
      };
    case "LOGIN_ERROR":
      return {
        ...state,
        errors: true,
      };
    default:
      return state;
  }
};

export default loginPageReducer;
