const initialState = {
  data: {},
  loading: true,
  error: null,
};

const weatherReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_WEATHER_REQUEST":
      return {
        ...state,
        data: {},
        loading: true,
        error: null,
      };
    case "FETCH_WEATHER_SUCCESS":
      return {
        ...state,
        data: action.weather,
        loading: false,
        error: null,
      };
    case "FETCH_WEATHER_FAILURE":
      return {
        ...state,
        data: {},
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default weatherReducer;
