import { call, put, takeLatest } from "redux-saga/effects";
import { weatherError, weatherLoaded } from "../actions";
import VacationService from "../../../services/vacation-service";

function* sagaWorker({ tripId, city }) {
  const vacationService = new VacationService();
  try {
    const result = {};
    result.tripid = tripId;
    result.city = city;
    const payload = yield call(vacationService.getWeatherFromUserId, result);
    yield put(weatherLoaded(payload.data));
  } catch (error) {
    yield put(weatherError(error));
  }
}

export function* weatherWatcher() {
  yield takeLatest("FETCH_WEATHER_REQUEST", sagaWorker);
}
