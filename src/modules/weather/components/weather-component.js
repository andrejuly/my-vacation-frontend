import React, { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import { useHistory } from "react-router-dom";
import { Empty } from "antd";
import Weather from "../../../components/weather/weather";

import Spinner from "../../../components/spinner";

const WeatherComponent = (props) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const history = useHistory();
  const { weather, loading, error, city, tripId } = props;

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    } else {
      history.push("/login");
    }
    if (userId) {
      props.fetchWeather(tripId, city);
    }
  }, [userId, authorized, city, history, tripId]); // eslint-disable-line react-hooks/exhaustive-deps

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />;
  }

  return <Weather weather={weather} />;
};

export default WeatherComponent;
