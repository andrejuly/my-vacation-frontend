const weatherLoaded = (weather) => ({
  type: "FETCH_WEATHER_SUCCESS",
  weather,
});

const weatherRequested = (tripId, city) => ({
  type: "FETCH_WEATHER_REQUEST",
  tripId,
  city,
});

const weatherError = (error) => ({
  type: "FETCH_WEATHER_FAILURE",
  error,
});

export { weatherRequested, weatherError, weatherLoaded };
