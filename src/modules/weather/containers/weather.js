import { connect } from "react-redux";
import { weatherRequested } from "../actions";
import WeatherComponent from "../components/weather-component";

const mapStateToProps = (state, props) => ({
  weather: state.weather.data,
  tripId: state.tripSelect.trip.id,
  loading: state.weather.loading,
  error: state.weather.error,
  city: props.city,
});

const mapDispatchToProps = (dispatch) => ({
  fetchWeather: (tripId, city) => dispatch(weatherRequested(tripId, city)),
});

const WeatherContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(WeatherComponent);
export default WeatherContainer;
