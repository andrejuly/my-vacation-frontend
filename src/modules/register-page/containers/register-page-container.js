import { connect } from "react-redux";
import { signupUser } from "../actions";
import RegistrationPage from "../components/registerPage";

const mapStateToProps = (state) => ({
  errors: state.registerPage.errors,
});

const mapDispatchToProps = (dispatch) => ({
  register: (values, successAction) => {
    dispatch(signupUser(values, successAction));
  },
});

const RegistrationPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationPage);

export default RegistrationPageContainer;
