const initialState = {
  user: null,
  errors: false,
};

const signupPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "USER_SIGNED_UP":
      return {
        ...state,
        user: action.user,
      };
    case "SIGNUP_ERROR":
      return {
        ...state,
        errors: true,
      };
    default:
      return state;
  }
};

export default signupPageReducer;
