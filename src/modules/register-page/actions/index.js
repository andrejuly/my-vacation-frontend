const signupUser = (values, successAction) => ({
  type: "SIGNUP_USER",
  values,
  successAction,
});

const userSignedUP = (user) => ({
  type: "USER_SIGNED_UP",
  user,
});

const signupError = () => ({
  type: "SIGNUP_ERROR",
});

export { signupError, signupUser, userSignedUP };
