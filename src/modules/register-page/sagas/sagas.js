import { call, put, takeLatest } from "redux-saga/effects";
import { signupError, userSignedUP } from "../actions";
import VacationService from "../../../services/vacation-service";

function* sagaWorker({ values, successAction }) {
  const service = new VacationService();
  try {
    const user = yield call(service.register, values);
    yield put(userSignedUP(user));
    successAction();
  } catch (error) {
    yield put(signupError(error));
  }
}

export function* signupWatcher() {
  yield takeLatest("SIGNUP_USER", sagaWorker);
}
