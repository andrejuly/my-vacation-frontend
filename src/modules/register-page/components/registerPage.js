import React from "react";
import { Alert, Button, DatePicker, Form, Input, Radio } from "antd";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const RegistrationPage = (props) => {
  const [form] = Form.useForm();
  const history = useHistory();
  const { t } = useTranslation();

  const { register, errors } = props;
  const onFinish = (values) => {
    const user = JSON.stringify(values, (key, value) =>
      key === "confirm" ? undefined : value
    );
    register(user, () => history.push("/login"));
  };

  return (
    <StyledRegistration>
      <StyledForm
        form={form}
        name="register"
        onFinish={onFinish}
        layout="vertical"
        scrollToFirstError
      >
        <Form.Item
          name="login"
          label={t("login.login")}
          tooltip={t("register.loginTooltip")}
          rules={[
            {
              required: true,
              message: t("login.alertLogin"),
              whitespace: true,
            },
          ]}
        >
          <StyledInput />
        </Form.Item>

        <Form.Item
          name="password"
          label={t("login.password")}
          rules={[
            {
              required: true,
              pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/,
              message: t("register.alertPassword"),
            },
          ]}
          hasFeedback
        >
          <StyledInputPassword />
        </Form.Item>

        <Form.Item
          name="confirm"
          label={t("register.confirm")}
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: t("login.alertPassword"),
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error(t("register.passwordDontMatch"))
                );
              },
            }),
          ]}
        >
          <StyledInputPassword />
        </Form.Item>

        <Form.Item
          name="fname"
          rules={[
            {
              required: true,
              type: "string",
              max: 20,
              message: t("register.nameMessage"),
            },
          ]}
          label={t("register.fname")}
        >
          <StyledInput />
        </Form.Item>

        <Form.Item name="birthDate" label={t("register.birthDate")}>
          <DatePicker placeholder={t("register.placeholderDate")} />
        </Form.Item>

        <Form.Item
          name="sex"
          rules={[
            {
              required: true,
              message: t("register.gender"),
            },
          ]}
          label={t("register.sex")}
        >
          <Radio.Group>
            <Radio value="m">{t("register.m")}</Radio>
            <Radio value="f">{t("register.w")}</Radio>
          </Radio.Group>
        </Form.Item>

        <Form.Item>
          <StyledButton type="primary" htmlType="submit">
            {t("register.button")}
          </StyledButton>
        </Form.Item>
        {errors && (
          <StyledAlert message={t("register.error")} type="error" showIcon />
        )}
      </StyledForm>
    </StyledRegistration>
  );
};

const StyledForm = styled(Form)`
  border-radius: 2px 2px 5px 5px;
  padding: 20px 20px 10px 20px;
  width: 90%;
  max-width: 440px;
  background: #ffffff;
  position: relative;
  padding-bottom: 3%;
  box-shadow: 0 1px 5px rgba(0, 0, 0, 0.3);
`;

const StyledRegistration = styled.div`
  font-family: "Montserrat", sans-serif;
  font-weight: 100;
  height: 100vh;
  background: url("https://trip-dreams.ru/wp-content/uploads/Marshrut-puteshestviya-po-Evrope.jpg")
    30% fixed;
  background-size: cover;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  min-height: 100%;
  padding: 20px;
`;

const StyledInput = styled(Input)`
  height: 30px;
  border: 1px solid #c4c4c4;
  font-size: 14px;
  font-style: normal;
`;

const StyledInputPassword = styled(Input.Password, Input)`
  height: 30px;
  border: 1px solid #c4c4c4;
  font-size: 14px;
  font-style: normal;
`;

const StyledButton = styled(Button)`
  border-radius: 20px;
  font-size: 16px;
  width: 200px;
  height: 3rem;
  margin-left: 100px;
`;

const StyledAlert = styled(Alert)`
  height: 50px;
  border: 1px solid #c4c4c4;
  font-size: 14px;
  font-style: normal;
`;

export default RegistrationPage;
