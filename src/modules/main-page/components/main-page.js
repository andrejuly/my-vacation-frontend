import React, { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import styled from "styled-components";
import { Link, Route, useHistory } from "react-router-dom";
import { Result, Button } from "antd";
import Spinner from "../../../components/spinner";
import Tab from "../../../components/tab/tab";
import MapContainer from "../../map/containers/map-container";
import TripsTableContainer from "../../trips-table/containers/trips-table-contatiner";
import SelectTripContainer from "../../select-trip/containers/select-trip-container";

const MainPage = (props) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const { login, loading, error } = props;
  const history = useHistory();
  const cookies = new Cookies();

  useEffect(() => {
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(cookies.get("user_id"));
    } else {
      history.push("/login");
    }
  }, [authorized, history]);

  useEffect(() => {
    if (userId) {
      props.fetchUserName(userId);
    }
  }, [userId, authorized]); // eslint-disable-line react-hooks/exhaustive-deps
  if (loading) return <Spinner />;
  if (error)
    return (
      <Result
        status="500"
        title="500"
        subTitle="Sorry, something went wrong."
        extra={
          <Link to="/login">
            <Button type="primary">Back Home</Button>
          </Link>
        }
      />
    );

  return (
    <StyledBack key="StyledBack">
      <StyledMain key="StyledMain">
        <Tab userName={login} />
        <Route path="/map" component={MapContainer} exact />
        <Route path="/trips" component={TripsTableContainer} exact />
        <Route path="/trip" component={SelectTripContainer} />
      </StyledMain>
    </StyledBack>
  );
};

const StyledBack = styled.div`
  background: #f6f5f3;
  height: 100%;
`;

const StyledMain = styled.div`
  max-width: 90%;
  margin: auto;
  height: 100%;
  background: white;
  font-family: "Montserrat", sans-serif;
  font-weight: 100;
`;

export default MainPage;
