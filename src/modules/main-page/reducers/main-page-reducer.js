const initialState = {
  login: "",
  loading: true,
  error: null,
};

const mainPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_USERNAME_REQUEST":
      return {
        ...state,
        login: "",
        loading: true,
        error: null,
      };
    case "FETCH_USERNAME_SUCCESS":
      return {
        ...state,
        login: action.login,
        loading: false,
        error: null,
      };
    case "FETCH_USERNAME_FAILURE":
      return {
        ...state,
        login: "",
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default mainPageReducer;
