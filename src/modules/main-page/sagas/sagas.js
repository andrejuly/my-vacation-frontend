import { call, put, takeLatest } from "redux-saga/effects";
import { loadLogin, loginError } from "../actions";
import VacationService from "../../../services/vacation-service";

const service = new VacationService();

function* sagaUserNameWorker({ userId }) {
  try {
    const result = {};
    result.userid = userId;
    const payload = yield call(service.getUserNameFromUserId, result);
    yield put(loadLogin(payload.data));
  } catch (error) {
    yield put(loginError(error));
  }
}

export function* userNameWatcher() {
  yield takeLatest("FETCH_USERNAME_REQUEST", sagaUserNameWorker);
}
