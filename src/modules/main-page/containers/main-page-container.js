import { connect } from "react-redux";
import { requestLogin } from "../actions";
import MainPage from "../components/main-page";

const mapStateToProps = ({ mainPage: { login, loading, error } }) => ({
  login,
  loading,
  error,
});

const mapDispatchToProps = (dispatch) => ({
  fetchUserName: (userId) => {
    dispatch(requestLogin(userId));
  },
});

const MainPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);

export default MainPageContainer;
