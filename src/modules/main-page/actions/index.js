export const requestLogin = (userId) => ({
  type: "FETCH_USERNAME_REQUEST",
  userId,
});

export const loadLogin = (login) => ({
  type: "FETCH_USERNAME_SUCCESS",
  login,
});

export const loginError = (error) => ({
  type: "FETCH_USERNAME_FAILURE",
  error,
});
