const initialState = {
  notes: {},
  pageNumber: 1,
  total: 0,
  loading: false,
  error: null,
};

const notesReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_NOTES_SUCCESS":
      return {
        ...state,
        notes: action.notes,
        pageNumber: action.pageNumber,
        total: action.total,
        loading: false,
        error: null,
      };
    case "FETCH_NOTES_FAILURE":
      return {
        ...state,
        notes: {},
        pageNumber: 1,
        total: 0,
        loading: false,
        error: action.error,
      };
    case "SEND_NOTES_SUCCESS":
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default notesReducer;
