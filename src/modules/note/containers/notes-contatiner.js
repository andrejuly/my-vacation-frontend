import { connect } from "react-redux";
import { requestNotes, sendNoteRequest } from "../actions";
import NoteComponent from "../components/note-components";

const mapStateToProps = (state) => ({
  notes: state.notes.notes,
  tripId: state.tripSelect.trip.id,
  username: state.mainPage.login,
  total: state.notes.total,
  pageNumber: state.notes.pageNumber,
});

const mapDispatchToProps = (dispatch) => ({
  fetchData: (tripId, pageNumber) => {
    dispatch(requestNotes(tripId, pageNumber));
  },
  sendData: (tripId, data) => {
    dispatch(sendNoteRequest(tripId, data));
  },
});

const NotesTableContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(NoteComponent);

export default NotesTableContainer;
