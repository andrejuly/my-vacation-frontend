const loadNotes = (notes, pageNumber, total) => ({
  type: "FETCH_NOTES_SUCCESS",
  notes,
  pageNumber,
  total,
});

const requestNotes = (tripId, pageNumber) => ({
  type: "FETCH_NOTES_REQUEST",
  tripId,
  pageNumber,
});

const errorNotes = (error) => ({
  type: "FETCH_NOTES_FAILURE",
  error,
});

export const sendNoteRequest = (tripId, data) => ({
  type: "SEND_NOTE_REQUEST",
  tripId,
  data,
});

export const sendNoteSuccess = () => ({
  type: "SEND_NOTE_SUCCESS",
});

export { loadNotes, errorNotes, requestNotes };
