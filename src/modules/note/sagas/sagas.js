import { call, put, takeLatest } from "redux-saga/effects";
import { errorNotes, loadNotes, sendNoteSuccess } from "../actions";
import VacationService from "../../../services/vacation-service";

const vacationService = new VacationService();

function* sagaWorker({ tripId, pageNumber }) {
  try {
    const result = {};
    result.tripid = tripId;
    result.page = pageNumber - 1;
    const payload = yield call(vacationService.getNotes, result);
    yield put(
      loadNotes(
        payload.data.content,
        payload.data.pageable.pageNumber,
        payload.data.totalElements
      )
    );
  } catch (error) {
    yield put(errorNotes(error));
  }
}

export function* notesWatcher() {
  yield takeLatest("FETCH_NOTES_REQUEST", sagaWorker);
}

function* sendNotesWorker({ tripId, data, pageNumber }) {
  try {
    const result = {};
    result.tripId = tripId;
    result.author = data.author;
    result.content = data.content;
    result.datetime = data.datetime;
    yield call(vacationService.addNote, result);
    yield put(sendNoteSuccess());
    const updateResult = {};
    updateResult.tripid = tripId;
    updateResult.page = pageNumber;
    const payload = yield call(vacationService.getNotes, updateResult);
    yield put(
      loadNotes(
        payload.data.content,
        payload.data.pageable.pageNumber,
        payload.data.totalElements
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* sendNotesWatcher() {
  yield takeLatest("SEND_NOTE_REQUEST", sendNotesWorker);
}
