import React, { useEffect, useState } from "react";
import { Button, Comment, Form, Input, List } from "antd";

import Cookies from "universal-cookie";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

const { TextArea } = Input;

const Editor = ({ onChange, onSubmit, submitting, value }) => {
  const { t } = useTranslation();
  return (
    <div style={{ display: "grid", justifyContent: "center" }}>
      <Form.Item>
        <TextArea
          allowClear
          rows={3}
          onChange={onChange}
          value={value}
          cols={40}
        />
      </Form.Item>
      <Form.Item style={{ margin: "0 auto" }}>
        <Button
          htmlType="submit"
          loading={submitting}
          onClick={onSubmit}
          type="primary"
        >
          {t("notes.button")}
        </Button>
      </Form.Item>
    </div>
  );
};

const NoteComponent = ({
  fetchData,
  notes,
  tripId,
  username,
  sendData,
  total,
}) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [value, setValue] = useState("");
  const { t } = useTranslation();

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(parseInt(cookies.get("user_id"), 10));
    }
    if (userId) {
      fetchData(tripId, pageNumber);
    }
  }, [authorized, userId, tripId]); // eslint-disable-line react-hooks/exhaustive-deps

  const onChange = (page) => {
    fetchData(tripId, page);
    setPageNumber(page);
  };

  const handleSubmit = () => {
    if (!value) {
      return;
    }
    setSubmitting(true);
    sendData(tripId, {
      author: username,
      content: value,
      datetime: Date.now(),
    });
    setPageNumber(1);
    setSubmitting(false);
  };

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  return (
    <div>
      {notes && notes.length ? (
        <List
          dataSource={notes}
          itemLayout="horizontal"
          header={<StyledHeader>{t("notes.title")}</StyledHeader>}
          pagination={{
            current: pageNumber,
            total,
            pageSize: 3,
            onChange,
          }}
          renderItem={(item) => (
            <li>
              <Comment
                author={item.author}
                content={item.content}
                datetime={new Date(item.datetime).toLocaleString("en-GB")}
              />
            </li>
          )}
        />
      ) : (
        <div
          style={{
            textAlign: "center",
            fontSize: "20px",
            borderBottom: "1px solid #e5e5e5",
            paddingBottom: "14px",
            paddingTop: "2px",
          }}
        >
          Notes
        </div>
      )}
      <Comment
        content={
          <Editor
            onChange={handleChange}
            onSubmit={handleSubmit}
            submitting={submitting}
            value={value}
          />
        }
      />
    </div>
  );
};

const StyledHeader = styled.div`
  font-size: 18px;
  text-align: center;
`;
export default NoteComponent;
