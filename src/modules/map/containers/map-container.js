import { connect } from "react-redux";
import { requestDestinations } from "../actions";
import MapTrip from "../components/map";

const mapStateToProps = ({
  tripSelect: {
    trip: { destinations },
  },
}) => ({
  destinations,
});

const mapDispatchToProps = (dispatch) => ({
  fetchDestinations: (userId) => {
    dispatch(requestDestinations(userId));
  },
});

const MapContainer = connect(mapStateToProps, mapDispatchToProps)(MapTrip);

export default MapContainer;
