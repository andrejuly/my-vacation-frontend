export const requestDestinations = (userId) => ({
  type: "FETCH_DESTINATIONS_REQUEST",
  userId,
});

export const loadDestinations = (destinations) => ({
  type: "FETCH_DESTINATIONS_SUCCESS",
  destinations,
});
