const initialState = {
  destinations: [],
};

const mapReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FETCH_DESTINATIONS_SUCCESS":
      return {
        ...state,
        destinations: action.destinations,
      };

    default:
      return state;
  }
};

export default mapReducer;
