import { call, put, takeLatest } from "redux-saga/effects";
import { loadDestinations } from "../actions";
import VacationService from "../../../services/vacation-service";

const service = new VacationService();

function* sagaWorker({ userId }) {
  try {
    const result = {};
    result.userid = userId;
    const payload = yield call(service.getDestinations, result);
    yield put(loadDestinations(payload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* mapWatcher() {
  yield takeLatest("FETCH_DESTINATIONS_REQUEST", sagaWorker);
}
