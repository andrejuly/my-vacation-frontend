import React, { useEffect, useRef, useState } from "react";
import { Map, YMaps } from "react-yandex-maps";
import { Redirect, useHistory } from "react-router-dom";
import Cookies from "universal-cookie";
import styled from "styled-components";

const MapTrip = (props) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const { destinations } = props;
  const history = useHistory();
  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      setUserId(cookies.get("user_id"));
    } else {
      history.push("/login");
    }
    if (userId) {
      props.fetchDestinations(userId);
    }
  }, [userId, authorized]); // eslint-disable-line react-hooks/exhaustive-deps
  const map = useRef(null);
  const mapState = {
    center: [55.739625, 37.5412],
    zoom: 12,
  };

  const apikey = "d2e63bba-58b9-4fc0-a200-7d4d2e1ccacb";
  const addRoute = (ymaps) => {
    const points = destinations.map((el) => [el.lat, el.lon]);
    const multiRoute = new ymaps.multiRouter.MultiRoute(
      {
        referencePoints: points,
        params: {
          routingMode: "auto",
        },
      },
      {
        boundsAutoApply: true,
      }
    );
    map.current.geoObjects.add(multiRoute);
  };
  return destinations ? (
    <StyledContentMap>
      <YMaps query={{ apikey }}>
        <StyledMap
          state={mapState}
          modules={["multiRouter.MultiRoute"]}
          instanceRef={map}
          onLoad={addRoute}
        />
      </YMaps>
    </StyledContentMap>
  ) : (
    <Redirect to="/trip" />
  );
};

const StyledMap = styled(Map)`
  height: 630px;
  width: 80%;
`;
const StyledContentMap = styled.div`
  display: flex;
  justify-content: center;
`;

export default MapTrip;
