import React from "react";
import { Spin } from "antd";
import styled from "styled-components";

const Spinner = () => (
  <StyledSpin>
    <Spin />
  </StyledSpin>
);

const StyledSpin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Spinner;
