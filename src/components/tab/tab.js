import React, { useCallback } from "react";

import { Menu, Tabs, Dropdown } from "antd";
import styled from "styled-components";
import { useHistory, Link } from "react-router-dom";
import { Icon } from "@iconify/react";
import { UserOutlined, GlobalOutlined } from "@ant-design/icons";
import flagForFlagRussia from "@iconify/icons-twemoji/flag-for-flag-russia";
import flagForFlagUnitedKingdom from "@iconify/icons-twemoji/flag-for-flag-united-kingdom";
import palmTree from "@iconify/icons-emojione-monotone/palm-tree";
import { useTranslation } from "react-i18next";
import "../../utils/i18n";
import Cookies from "universal-cookie";

import CreateTripContainer from "../../modules/create-trip/containers/create-trip-container";

import { disconnect } from "../../services/web-socket-service";

const { TabPane } = Tabs;
const { SubMenu } = Menu;

const cookies = new Cookies();

const Tab = ({ userName }) => {
  const history = useHistory();
  const { t, i18n } = useTranslation();
  const changeLanguage = (lang) => {
    i18n.changeLanguage(lang);
  };
  const handleTabClick = useCallback((key) => {
    history.push(`/${key}`);
  }, []);

  const logOut = useCallback(() => {
    cookies.remove("token");
    cookies.remove("user_id");
    disconnect();
  }, []);

  const menu = (
    <Menu>
      <Menu.Item key="en" onClick={() => changeLanguage("en")}>
        <Icon icon={flagForFlagUnitedKingdom} /> English
      </Menu.Item>
      <Menu.Item key="ru" onClick={() => changeLanguage("ru")}>
        <Icon icon={flagForFlagRussia} /> Russian
      </Menu.Item>
    </Menu>
  );

  const extraContent = {
    left: (
      <div>
        <StyledIcon icon={palmTree} />
        <StyledName>My Vacation</StyledName>
      </div>
    ),
    right: (
      <StyledExtra>
        <StyledCreate>
          <CreateTripContainer />
        </StyledCreate>
        <Menu
          key="menu"
          mode="horizontal"
          triggerSubMenuAction="click"
          onClick={(e) => {
            if (e.key === "sign out") {
              logOut();
            }
          }}
        >
          <StyledSubMenu key="subMenu" title={userName} icon={<UserOutlined />}>
            <Menu.Item key="sign out">
              <StyledExit>
                <Link to="/login">{t("tabPanel.signOut")}</Link>
              </StyledExit>
            </Menu.Item>
          </StyledSubMenu>
        </Menu>
        <Dropdown overlay={menu} placement="bottomRight">
          <StyledGlobalOutlined />
        </Dropdown>
      </StyledExtra>
    ),
  };
  return (
    <StyledTab
      centered
      defaultActiveKey="trip"
      tabBarExtraContent={extraContent}
      onChange={handleTabClick}
    >
      <TabPane tab={t("tabPanel.trip")} key="trip" />
      <TabPane tab={t("tabPanel.map")} key="map" />
      <TabPane tab={t("tabPanel.allTrips")} key="trips" />
    </StyledTab>
  );
};

const StyledTab = styled(Tabs)`
  padding-left: 1rem;
`;

const StyledIcon = styled(Icon)`
  margin-top: 20px;
  font-size: 32px;
  color: black;
  background: white;
`;

const StyledName = styled.p`
  font-size: 18px;
  margin-left: 10px;
  vertical-align: middle;
  display: inline-block;
`;

const StyledExtra = styled.div`
  display: flex;
  align-items: center;
  margin: 0 -5px;
`;

const StyledExit = styled.div`
  display: flex;
  font-size: 16px;
  justify-content: center;
`;

const StyledSubMenu = styled(SubMenu)`
  width: 140px;
`;

const StyledCreate = styled.div`
  margin-top: 10px;
`;

const StyledGlobalOutlined = styled(GlobalOutlined)`
  font-size: 16px;
  color: #08c;
  margin-right: 10px;
`;

export default Tab;
