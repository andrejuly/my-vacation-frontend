import React from "react";
import { List } from "antd";
import { Scrollbars } from "react-custom-scrollbars";

const News = ({ data }) => (
  <Scrollbars style={{ height: "200px", width: "90%" }}>
    <List
      dataSource={data}
      renderItem={(item) => (
        <List.Item key={item.url}>
          <List.Item.Meta
            title={<a href={item.url}>{item.title}</a>}
            description={item.description}
          />
        </List.Item>
      )}
    />
  </Scrollbars>
);

export default News;
