import React from "react";
import { Avatar } from "antd";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import "../../utils/i18n";
import Spinner from "../spinner";

const Weather = ({ weather }) => {
  const { t } = useTranslation();
  return weather && weather.main ? (
    <div>
      <StyledCityName>
        {t("weather.title")} <br /> {t("weather.inCity")} {weather.name}
      </StyledCityName>
      <StyledTemp>
        {`${Math.round(weather.main.temp)} °C`}
        <StyledDescription>
          {weather.weather[0].description}
          <Avatar
            src={`http://openweathermap.org/img/w/${weather.weather[0].icon}.png`}
            size="large"
          />
          <br />
          {t("weather.humidity")} {weather.main.humidity}%<br />
          {t("weather.speed")} {weather.wind.speed} м/c
        </StyledDescription>
      </StyledTemp>
    </div>
  ) : (
    <Spinner />
  );
};

const StyledCityName = styled.p`
  font-size: 24px;
  text-align: center;
`;
const StyledTemp = styled.div`
  display: flex;
  align-items: center;
  font-size: 32px;
  white-space: nowrap;
`;
const StyledDescription = styled.div`
  font-size: 14px;
  margin-left: 30px;
`;

export default Weather;
