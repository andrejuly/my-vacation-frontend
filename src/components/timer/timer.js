import { Statistic } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";

const { Countdown } = Statistic;

const Timer = ({ startDate }) => {
  const { t } = useTranslation();
  return Date.now() - startDate < 0 ? (
    <Countdown value={startDate} format={t("timer.format")} />
  ) : (
    <p>{t("timer.title")}</p>
  );
};

export default Timer;
