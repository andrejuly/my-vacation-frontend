import React, { Suspense, useEffect } from "react";
import "./app.css";
import Cookies from "universal-cookie";
import { Route, Switch, Redirect } from "react-router-dom";
import LoginPageContainer from "../../modules/login-page/containers/login-page-container";
import MainPageContainer from "../../modules/main-page/containers/main-page-container";
import RegistrationPageContainer from "../../modules/register-page/containers/register-page-container";
import Spinner from "../spinner";
import { connect } from "../../services/web-socket-service";

const App = () => {
  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      connect();
    }
  }, []);
  return (
    <Suspense fallback={<Spinner />}>
      <Switch>
        <Route path="/trip" component={MainPageContainer} />
        <Route path="/map" component={MainPageContainer} exact />
        <Route path="/trips" component={MainPageContainer} exact />
        <Route path="/login" component={LoginPageContainer} exact />
        <Route
          path="/registration"
          component={RegistrationPageContainer}
          exact
        />
        <Route render={() => <Redirect to={{ pathname: "/trip" }} />} />
      </Switch>
    </Suspense>
  );
};

export default App;
