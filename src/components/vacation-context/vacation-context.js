import React from "react";

const { Provider: VacationServiceProvider, Consumer: VacationServiceConsumer } =
  React.createContext();

export { VacationServiceProvider, VacationServiceConsumer };
