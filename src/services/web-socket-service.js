import Stomp from "stompjs";
import Cookies from "universal-cookie";
import { notification } from "antd";

let stompClient = null;

export function connect() {
  const cookies = new Cookies();
  // eslint-disable-next-line no-undef
  const socket = new SockJS("http://localhost:8080/websocket");
  stompClient = Stomp.over(socket);
  stompClient.connect({}, () => {
    stompClient.subscribe(
      `/user/${cookies.get("user_id")}/queue/messages`,
      (message) => {
        notification.open({
          message: message.body,
          duration: 10,
        });
      }
    );
  });
}

export function disconnect() {
  if (stompClient !== null) {
    stompClient.disconnect();
  }
}
