/* eslint class-methods-use-this: 0 */ // --> OFF
import React from "react";
import axios from "axios";
import Cookies from "universal-cookie";

axios.defaults.headers.post["Content-Type"] = "application/json";

const cookies = new Cookies();

class VacationService extends React.Component {
  // eslint-disable-next-line react/sort-comp
  async register(values) {
    return axios.post(`http://localhost:8080/authorization/register`, values);
  }

  async login(values) {
    return axios.post(`http://localhost:8080/authorization/login`, values);
  }

  async getTripFromUserId(params) {
    return axios.get("http://localhost:8080/trip/userid", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async getTripById(id) {
    return axios.get(`http://localhost:8080/trip/${id}`);
  }

  async getDestinations(params) {
    return axios.get("http://localhost:8080/trip/destinations", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async getUserNameFromUserId(params) {
    return axios.get("http://localhost:8080/username", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async getWeatherFromUserId(params) {
    return axios.get("http://localhost:8080/trip/weather", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async getNewsFromUserId(params) {
    return axios.get("http://localhost:8080/trip/news", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async getTripsFromUserId(params) {
    return axios.get("http://localhost:8080/usertrips", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async patchTrip(trip) {
    return axios.post("http://localhost:8080/statustrip", trip);
  }

  async addTrip(trip) {
    return axios.post("http://localhost:8080/addtrip", trip);
  }

  async getCitiesForTrip() {
    return axios.get("http://localhost:8080/cities");
  }

  async getTrips(params) {
    return axios.get("http://localhost:8080/trips", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async follow(values) {
    return axios.post("http://localhost:8080/subscriptions/add", values);
  }

  async unfollow(values) {
    return axios.post("http://localhost:8080/subscriptions/delete", values);
  }

  async getNotes(params) {
    return axios.get("http://localhost:8080/notes/getbytrip", {
      params,
    });
  }

  async getSubscriptions(params) {
    return axios.get("http://localhost:8080/subscriptions/allsubscriptions", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async getUserInfo(params) {
    return axios.get("http://localhost:8080/user", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  async addNote(values) {
    return axios.post("http://localhost:8080/notes/add", values);
  }
}

export default VacationService;
